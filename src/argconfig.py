"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

import argparse

def parse_args(ap):
    """
    Provides command line arguments to inference.py

    Args:
        ap (argparse.ArgumentParser): Argument parser class

    Returns:
        args (argparse.ArgumentParser): Argument parser class with added arguments
    """
    ap.add_argument("-p", "--platform", required=True, help="Compiling and storing model locally or on Alibaba Cloud")

    ap.add_argument("-ib", "--input_bucket",help="Input OSS bucket name")
    ap.add_argument("-ob", "--output_bucket", help="Output OSS bucket name")
        
    ap.add_argument("-if", "--input_file", required=True, help="Input file name")
    ap.add_argument("-of", "--output_file", required=True, help="Output file name")
    args = vars(ap.parse_args())
    return args