"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
main.py:
    Entry point to run complition for ml model
"""
import sys
import os
import subprocess
import json
import argparse
from flask import Flask
from flask import send_file
from src.argconfig import parse_args
from src.alibaba_agent import AlibabaAgent
from src.aws_agent import AwsAgent
from src.onnx_to_tvm import onnx_to_tvm

app = Flask(__name__)
ap = argparse.ArgumentParser()
args = parse_args(ap)
    
def save_model_locally():
    """
    Starts flask ui to be able to server the compiled ".so" TVM model

    Args:
        None
    
    Returns:
        None
    """
    print("The model is saved locally... Please copy the below address and paste in browser to download compiled model")
    print("====================================================")
    print("MODEL DOWNLOAD LINK: http://127.0.0.1:8060/download")
    print("====================================================")
    from waitress import serve
    serve(app, host="0.0.0.0", port=8060)
    #app.run(port=8060)


@app.route('/download')
def downloadFile():
    path = args["output_file"]
    return send_file(path, as_attachment=True)

if __name__=="__main__":
    if args["platform"] == "alibaba_cloud":
        #Uploads built TVM model to alibaba cloud
        cloud_agent = AlibabaAgent()
        cloud_agent.download(args["input_bucket"], args["input_file"], "trained_model/fused_model.onnx")
        onnx_to_tvm("trained_model/fused_model.onnx", "compiled_model.tar.gz")
        cloud_agent.upload(args["output_bucket"], "compiled_model.tar.gz", args["output_file"])
        
    elif args["platform"] == "aws":
        #Uploads built TVM model to aws bucket
        cloud_agent = AwsAgent()
        print("Agent_selected")
        cloud_agent.download_from_s3(args["input_bucket"], args["input_file"], "trained_model/fused_model.onnx")
        print("Downloaded model...Starting compilation.....")
        onnx_to_tvm("trained_model/fused_model.onnx", "compiled_model.tar.gz")
        cloud_agent.upload_to_s3(args["output_bucket"], args["output_file"], "compiled_model.tar.gz")
        
    elif args["platform"] == "local":
        #Uploads built TVM model locally via flask 
        onnx_to_tvm(args["input_file"], args["output_file"])
        save_model_locally()
        
    else:
        print("Invalid_option...")

