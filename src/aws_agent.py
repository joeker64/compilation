"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
aws_agent.py:
    Applying the configuraion for uploading files to aws bucket
"""
import boto3
from tqdm import tqdm
import os
import json

class AwsAgent:
    def __init__(self):
        with open("config/aws_config.json") as json_data_file:
            aws_data = json.load(json_data_file)
        self.s3 = boto3.client('s3', 
                        aws_access_key_id=aws_data["access_key"], 
                        aws_secret_access_key=aws_data["secret_key"], 
                        region_name=aws_data["region"]
                        )
        
    def download_from_s3(self, bucket, key, filename):
        meta_data = self.s3.head_object(Bucket=bucket, Key=key)
        total_length = int(meta_data.get('ContentLength', 0))
        print(f"Downloading the model : {key} From bucket : {bucket}, File size : {round(total_length/1024/1024)} MB")
        with tqdm(total=total_length,  desc=f'source: s3://{bucket}/{key}', 
            bar_format="{percentage:.1f}%|{bar:25} | {rate_fmt} | {desc}",  unit='B', unit_scale=True, unit_divisor=1024) as pbar:
            self.s3.download_file(bucket, key, Filename=filename, Callback=pbar.update)

    def upload_to_s3(self, bucket, key, filename):
        file_size = os.stat(filename).st_size
        print(f"Uploading the model : {filename} To bucket : {bucket}, File size : {round(file_size/1024/1024, 3)} MB")
        with tqdm(total=file_size, bar_format="{percentage:.1f}%|{bar:50} | {rate_fmt} | {desc}" ,unit="B", unit_scale=True, desc=filename) as pbar:
            self.s3.upload_file(Filename=filename,Bucket=bucket,Key=key,
                        Callback=lambda bytes_transferred: pbar.update(bytes_transferred),
                        )