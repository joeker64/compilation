FROM python:3.8-slim-buster

RUN apt-get update && \ 
    apt-get install -y python3 python3-dev python3-pip python3-setuptools python3-wheel \
                        gcc libtinfo-dev zlib1g-dev build-essential cmake libedit-dev libxml2-dev \
                        libgl1-mesa-glx libssl-dev git wget llvm clang libsm6 libxext6 ocl-icd-opencl-dev gpg

RUN python3 -m pip install --upgrade pip

RUN python3 -m pip install apache-tvm
RUN python3 -m pip install waitress
RUN python3 -m pip install boto3 oss2 flask 
RUN python3 -m pip install onnx onnxruntime  
RUN python3 -m pip install decorator scipy psutil attrs 
RUN python3 -m pip install tqdm

WORKDIR /
COPY src/ src/
COPY trained_model/ trained_model/
COPY config/ config/
COPY main.py main.py

ENTRYPOINT ["python3", "main.py"]
